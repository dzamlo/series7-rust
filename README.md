# series7

This repositories contains rust code to parse Xilinx 7 Series FPGA bitstream as documented in UG470. There is some code to run Vivado too.

## Example

The provided `parse-stdin` tool print the parsed contents of a bitstream file to stdout. You can run it like that:

```bash
cargo run --bin parse-stdin -- register-writes < bitstream.bit
```

