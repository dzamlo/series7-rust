use nom;
use nom::be_u16;
use nom::be_u32;
use std::io;
use std::io::Read;

#[cfg_attr(rustfmt, rustfmt_skip)]
named!(
    string<String>,
    do_parse!(
        length: be_u16 >>
        str: map_res!(take!(length - 1), |bytes: &[u8]| {
            String::from_utf8(bytes.to_vec())
        }) >>
        tag!("\x00") >> (str)
    )
);

named!(
    vec<Vec<u8>>,
    map!(length_data!(be_u32), |bytes| bytes.to_vec())
);

#[cfg_attr(rustfmt, rustfmt_skip)]
named!(
    bitfile_header<BitFileHeader>,
    do_parse!(
        length_data!(be_u16) >>
        tag!(b"\x00\x01a") >>
        design_name: string >>
        tag!("b") >>
        part_name: string >>
        tag!("c") >>
        date: string >>
        tag!("d") >>
        time: string >>
        (BitFileHeader {design_name, part_name, date, time})
    )
);

#[cfg_attr(rustfmt, rustfmt_skip)]
named!(
    bitfile_parser<BitFile>,
    do_parse!(
        header: bitfile_header >>
        tag!("e") >>
        raw_bitstream: vec >>
        (BitFile {header,raw_bitstream})
    )
);

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct BitFileHeader {
    pub design_name: String,
    pub part_name: String,
    pub date: String,
    pub time: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct BitFile {
    pub header: BitFileHeader,
    pub raw_bitstream: Vec<u8>,
}

pub fn header_from_reader(mut reader: impl Read) -> Result<BitFileHeader, FromReaderError> {
    let mut input = Vec::new();
    reader.read_to_end(&mut input)?;
    let (_, bitfile_header) = bitfile_header(&input)?;
    Ok(bitfile_header)
}

pub fn bitfile_from_reader(mut reader: impl Read) -> Result<BitFile, FromReaderError> {
    let mut input = Vec::new();
    reader.read_to_end(&mut input)?;
    let (_, bitfile) = bitfile_parser(&input)?;
    Ok(bitfile)
}

#[derive(Debug, Fail)]
pub enum FromReaderError {
    #[fail(display = "io error: {}", _0)]
    IoError(#[cause] io::Error),
    #[fail(display = "parse error")]
    ParseError,
}

impl From<io::Error> for FromReaderError {
    fn from(value: io::Error) -> FromReaderError {
        FromReaderError::IoError(value)
    }
}

impl<T> From<nom::Err<T>> for FromReaderError {
    fn from(_value: nom::Err<T>) -> FromReaderError {
        FromReaderError::ParseError
    }
}
