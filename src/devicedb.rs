use bitstream::register::FrameAddress;

pub struct Device {
    name: String,
    idcode: u32,
    part_names: Vec<String>,
    addresses: Vec<Option<FrameAddress>>,
}
