use byteorder::BigEndian;
use byteorder::ByteOrder;
use std::mem::size_of;

pub fn u8_slice_to_u32(input: &[u8]) -> Vec<u32> {
    let rounded_length = input.len() - input.len() % size_of::<u32>();
    let mut buf = vec![0; input.len() / size_of::<u32>()];
    BigEndian::read_u32_into(&input[..rounded_length], &mut buf);
    buf
}

macro_rules! open_consts_enum {
    ($enum_name:ident, $values_type:ident {
        $($const_name:ident = $const_value:expr,)*
    }) => (
        #[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
        #[allow(non_camel_case_types)]
        pub enum $enum_name {
            $($const_name,)*
            Other($values_type)
        }

        impl $enum_name {
            #[allow(unused)]
            pub fn from(value: $values_type) -> $enum_name {
                match value {
                    $($const_value => $enum_name::$const_name,)*
                    value => $enum_name::Other(value),
                }
            }
        }

        impl From<$values_type> for $enum_name {
            fn from(value: $values_type) -> $enum_name {
                match value {
                    $($const_value => $enum_name::$const_name,)*
                    value => $enum_name::Other(value)
                }
            }
        }

        impl From<$enum_name> for $values_type {
            fn from(value: $enum_name) -> $values_type {
                match value {
                    $($enum_name::$const_name => $const_value,)*
                    $enum_name::Other(value) => value,
                }
            }
        }
    );
}
