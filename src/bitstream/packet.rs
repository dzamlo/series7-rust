use super::raw_packet::Opcode;
use super::register::RegisterAddress;
use bitstream::RawPacket;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Packet {
    Nop,
    Read {
        register_address: RegisterAddress,
        word_count: u32,
    },
    Write {
        register_address: RegisterAddress,
        data: Vec<u32>,
    },
}

impl Packet {
    pub fn from_raw_packets<T: Iterator<Item = RawPacket>>(iter: T) -> Vec<Packet> {
        // A valid bitstream is composed of type 1 and type 2 RawPacket.
        // If a type 1 is followed by a type 2, the type 1 is only here to indicate the register
        // address.

        let mut packets = vec![];

        let mut raw_packets = iter.peekable();

        let mut previous_register_address = RegisterAddress::from(0);

        while let Some(raw_packet) = raw_packets.next() {
            match raw_packets.peek() {
                Some(next_raw_packet) if next_raw_packet.header.is_type2() => {
                    if let Some(register_address) = raw_packet.header.register_address() {
                        previous_register_address = register_address
                    }
                }
                None | Some(_) => match raw_packet.header.opcode() {
                    Opcode::Nop => packets.push(Packet::Nop),
                    Opcode::Read => packets.push(Packet::Read {
                        register_address: raw_packet
                            .header
                            .register_address()
                            .unwrap_or(previous_register_address),
                        word_count: raw_packet.header.word_count(),
                    }),
                    Opcode::Write => packets.push(Packet::Write {
                        register_address: raw_packet
                            .header
                            .register_address()
                            .unwrap_or(previous_register_address),
                        data: raw_packet.payload,
                    }),
                    Opcode::Reserved => {}
                },
            }
        }

        packets
    }
}
