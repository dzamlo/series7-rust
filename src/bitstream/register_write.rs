use super::register::{
    Command, ConfigurationOptions0, ConfigurationOptions1, Control0, FrameAddress, RegisterAddress,
    WarmBootStartAdress, WatchdogTimer,
};
use bitstream::Packet;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum RegisterWrite {
    Crc(u32),
    Far(FrameAddress),
    Fdri(Vec<u32>),
    Cmd(Command),
    Ctl0(Control0),
    Mask(u32),
    Lout(u32),
    Cor0(ConfigurationOptions0),
    Mfwr(Vec<u32>),
    Cbc(Vec<u32>),
    Idcode(u32),
    Axss(Vec<u32>),
    Cor1(ConfigurationOptions1),
    Wbstar(WarmBootStartAdress),
    Timer(WatchdogTimer),
    Ctl1(u32),
    Bspi(u32),
    BspiRead(u32),
    FallEdge,
    Other {
        address: RegisterAddress,
        data: Vec<u32>,
    },
}

impl RegisterWrite {
    pub fn new(
        address: RegisterAddress,
        data: Vec<u32>,
    ) -> Result<RegisterWrite, NewRegisterWriteError> {
        let register_write = match address {
            RegisterAddress::CRC => RegisterWrite::Crc(getu32(address, &data)?),
            RegisterAddress::FAR => RegisterWrite::Far(FrameAddress(getu32(address, &data)?)),
            RegisterAddress::FDRI => RegisterWrite::Fdri(data),
            RegisterAddress::CMD => RegisterWrite::Cmd(Command::from(getu32(address, &data)?)),
            RegisterAddress::CTL0 => RegisterWrite::Ctl0(Control0(getu32(address, &data)?)),
            RegisterAddress::MASK => RegisterWrite::Mask(getu32(address, &data)?),
            RegisterAddress::LOUT => RegisterWrite::Lout(getu32(address, &data)?),
            RegisterAddress::COR0 => {
                RegisterWrite::Cor0(ConfigurationOptions0(getu32(address, &data)?))
            }
            RegisterAddress::MFWR => RegisterWrite::Mfwr(data),
            RegisterAddress::CBC => RegisterWrite::Cbc(data),
            RegisterAddress::IDCODE => RegisterWrite::Idcode(getu32(address, &data)?),
            RegisterAddress::AXSS => RegisterWrite::Axss(data),
            RegisterAddress::COR1 => {
                RegisterWrite::Cor1(ConfigurationOptions1(getu32(address, &data)?))
            }
            RegisterAddress::WBSTAR => {
                RegisterWrite::Wbstar(WarmBootStartAdress(getu32(address, &data)?))
            }
            RegisterAddress::TIMER => RegisterWrite::Timer(WatchdogTimer(getu32(address, &data)?)),
            RegisterAddress::CTL1 => RegisterWrite::Ctl1(getu32(address, &data)?),
            RegisterAddress::BSPI => RegisterWrite::Bspi(getu32(address, &data)?),
            RegisterAddress::BSPI_READ => RegisterWrite::BspiRead(getu32(address, &data)?),
            RegisterAddress::FALL_EDGE => RegisterWrite::FallEdge,

            r => RegisterWrite::Other { address: r, data },
        };

        Ok(register_write)
    }

    pub fn from_packets<T: Iterator<Item = Packet>>(
        iter: T,
    ) -> Result<Vec<RegisterWrite>, NewRegisterWriteError> {
        iter.filter_map(|p| match p {
            Packet::Write {
                register_address,
                data,
            } => Some((register_address, data)),
            _ => None,
        })
        .map(|(register_address, data)| RegisterWrite::new(register_address, data))
        .collect()
    }
}

fn getu32(address: RegisterAddress, data: &[u32]) -> Result<u32, NewRegisterWriteError> {
    if data.len() == 1 {
        Ok(data[0])
    } else {
        Err(NewRegisterWriteError::InvalidDataLength {
            actual_length: data.len(),
            expected_length: 1,
            register_address: address,
        })
    }
}

#[derive(Debug, Fail)]
pub enum NewRegisterWriteError {
    #[fail(
        display = "invalid data length: expected {} got {}",
        expected_length, actual_length
    )]
    InvalidDataLength {
        actual_length: usize,
        expected_length: usize,
        register_address: RegisterAddress,
    },
}
