use std::io::{self, Read};
use utils::u8_slice_to_u32;

pub mod packet;
pub mod raw_packet;
pub mod register;
pub mod register_write;

pub use self::packet::Packet;
pub use self::raw_packet::{ParseError, RawPacket};
pub use self::register_write::RegisterWrite;
use bitstream::register_write::NewRegisterWriteError;

pub const SYNC_WORD: [u8; 4] = [0xAA, 0x99, 0x55, 0x66];

/// Find the sync word.
///
/// If found, it will return the index after the sync word.
pub fn find_sync_word(bitstream: &[u8]) -> Option<usize> {
    if bitstream.len() < SYNC_WORD.len() {
        None
    } else {
        for i in 0..bitstream.len() - SYNC_WORD.len() {
            if bitstream[i..i + SYNC_WORD.len()] == SYNC_WORD {
                return Some(i + SYNC_WORD.len());
            }
        }
        None
    }
}

pub fn register_writes_from_reader<R: Read>(
    reader: &mut R,
) -> Result<Vec<RegisterWrite>, FromReaderError> {
    let packets = packets_from_reader(reader)?;
    let register_writes = RegisterWrite::from_packets(packets.into_iter())?;
    Ok(register_writes)
}

pub fn packets_from_reader<R: Read>(reader: &mut R) -> Result<Vec<Packet>, FromReaderError> {
    let raw_packets = raw_packets_from_reader(reader)?;
    let packets = Packet::from_raw_packets(raw_packets.into_iter());
    Ok(packets)
}

pub fn raw_packets_from_reader<R: Read>(reader: &mut R) -> Result<Vec<RawPacket>, FromReaderError> {
    let mut bitstream = vec![];
    reader.read_to_end(&mut bitstream)?;
    let bitstream_start = find_sync_word(&bitstream).ok_or(FromReaderError::SyncWordNotFound)?;
    let bitstream = u8_slice_to_u32(&bitstream[bitstream_start..]);
    let mut iter = bitstream.into_iter();
    let raw_packets = RawPacket::parse_all(&mut iter)?;
    Ok(raw_packets)
}

#[derive(Debug, Fail)]
pub enum FromReaderError {
    #[fail(display = "sync word not found")]
    SyncWordNotFound,
    #[fail(display = "io error: {}", _0)]
    IoError(#[cause] io::Error),
    #[fail(display = "parse error: {}", _0)]
    ParseError(#[cause] ParseError),
    #[fail(display = "register write error: {}", _0)]
    NewRegisterWriteError(#[cause] NewRegisterWriteError),
}

impl From<io::Error> for FromReaderError {
    fn from(value: io::Error) -> FromReaderError {
        FromReaderError::IoError(value)
    }
}

impl From<ParseError> for FromReaderError {
    fn from(value: ParseError) -> FromReaderError {
        FromReaderError::ParseError(value)
    }
}

impl From<NewRegisterWriteError> for FromReaderError {
    fn from(value: NewRegisterWriteError) -> FromReaderError {
        FromReaderError::NewRegisterWriteError(value)
    }
}
