use super::register::RegisterAddress;

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Type1Header(u32);
    impl Debug;
    pub header_type, set_header_type: 31, 29;
    pub into Opcode, opcode, set_raw_opcode: 28, 27;
    pub into RegisterAddress, register_address, set_register_address: 26, 13;
    pub word_count, set_word_count: 10, 0;
}

impl Type1Header {
    pub fn set_opcode(&mut self, opcode: Opcode) {
        self.set_raw_opcode(opcode as u32);
    }
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Type2Header(u32);
    impl Debug;
    pub header_type, set_header_type: 31, 29;
    pub into Opcode, opcode, set_raw_opcode: 28, 27;
    pub word_count, set_word_count: 26, 0;
}

impl Type2Header {
    pub fn set_opcode(&mut self, opcode: Opcode) {
        self.set_raw_opcode(opcode as u32);
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Header {
    Type1(Type1Header),
    Type2(Type2Header),
    Other(u32),
}

impl From<u32> for Header {
    fn from(value: u32) -> Header {
        match Type1Header(value).header_type() {
            1 => Type1Header(value).into(),
            2 => Type2Header(value).into(),
            _ => Header::Other(value),
        }
    }
}

impl From<Type1Header> for Header {
    fn from(value: Type1Header) -> Header {
        Header::Type1(value)
    }
}

impl From<Type2Header> for Header {
    fn from(value: Type2Header) -> Header {
        Header::Type2(value)
    }
}

impl Header {
    pub fn word_count(&self) -> u32 {
        match *self {
            Header::Type1(header) => header.word_count(),
            Header::Type2(header) => header.word_count(),
            Header::Other(..) => 0,
        }
    }

    pub fn opcode(&self) -> Opcode {
        match *self {
            Header::Type1(header) => header.opcode(),
            Header::Type2(header) => header.opcode(),
            Header::Other(..) => Opcode::Reserved,
        }
    }

    pub fn is_type2(&self) -> bool {
        match *self {
            Header::Type2(_) => true,
            _ => false,
        }
    }

    pub fn register_address(&self) -> Option<RegisterAddress> {
        match *self {
            Header::Type1(header) => Some(header.register_address()),
            _ => None,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Opcode {
    Nop = 0b00,
    Read = 0b01,
    Write = 0b10,
    Reserved = 0b11,
}

impl From<u32> for Opcode {
    fn from(value: u32) -> Opcode {
        match value {
            0b00 => Opcode::Nop,
            0b01 => Opcode::Read,
            0b10 => Opcode::Write,
            _ => Opcode::Reserved,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct RawPacket {
    pub header: Header,
    pub payload: Vec<u32>,
}

impl RawPacket {
    pub fn parse<T: Iterator<Item = u32>>(iter: &mut T) -> Result<RawPacket, ParseError> {
        let header: Header = iter.next().ok_or(ParseError::EmptyIterator)?.into();
        let payload: Vec<_> = iter.take(header.word_count() as usize).collect();
        if payload.len() != header.word_count() as usize {
            Err(ParseError::PayloadTooShort {
                actual_length: payload.len(),
                expected_length: header.word_count() as usize,
                header,
            })
        } else {
            Ok(RawPacket { header, payload })
        }
    }

    pub fn parse_all<T: Iterator<Item = u32>>(iter: &mut T) -> Result<Vec<RawPacket>, ParseError> {
        let mut packets = vec![];
        loop {
            match RawPacket::parse(iter) {
                Ok(packet) => packets.push(packet),
                Err(ParseError::EmptyIterator) => break,
                Err(err) => return Err(err),
            }
        }

        Ok(packets)
    }
}

#[derive(Debug, Fail)]
pub enum ParseError {
    #[fail(display = "the iterator was empty")]
    EmptyIterator,
    #[fail(
        display = "payload too short: expected {} got {}",
        expected_length, actual_length
    )]
    PayloadTooShort {
        actual_length: usize,
        expected_length: usize,
        header: Header,
    },
}
