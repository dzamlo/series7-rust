open_consts_enum! {
    RegisterAddress, u32 {
        CRC = 0b00000,
        FAR = 0b00001,
        FDRI = 0b00010,
        FDRO = 0b00011,
        CMD = 0b00100,
        CTL0 = 0b00101,
        MASK = 0b00110,
        STAT = 0b00111,
        LOUT = 0b01000,
        COR0 = 0b01001,
        MFWR = 0b01010,
        CBC = 0b01011,
        IDCODE = 0b01100,
        AXSS = 0b01101,
        COR1 = 0b01110,
        WBSTAR = 0b10000,
        TIMER = 0b10001,
        BOOTSTS = 0b10110,
        CTL1 = 0b11000,
        BSPI = 0b11111,
        BSPI_READ = 0b10010,
        FALL_EDGE = 0b10011,
    }
}

open_consts_enum! {
    BlockType, u32 {
        CLB_IO_CLK = 0b000,
        BLOCK_RAM = 0b001,
        CFG_CLB = 0b010,
    }
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct FrameAddress(u32);
    impl Debug;
    pub into BlockType, block_type, set_raw_block_type: 25, 23;
    pub bottom, set_bottom: 22;
    pub row_address, set_row_address: 21, 17;
    pub column_address, set_column_address: 16, 7;
    pub minor_address, set_minor_address: 6, 0;
}

open_consts_enum! {
    Command, u32 {
        NULL = 0b00000,
        WCFG = 0b00001,
        MFW = 0b00010,
        DGHIGH_LFRM = 0b00011,
        RCFG = 0b00100,
        START = 0b00101,
        RCAP = 0b00110,
        RCRC = 0b00111,
        AGHIGH = 0b01000,
        SWITCH = 0b01001,
        GRESTORE = 0b01010,
        SHUTDOWN = 0b01011,
        GCAPTURE = 0b01100,
        DESYNC = 0b01101,
        Reserved = 0b01110,
        IPROG = 0b01111,
        CRCC = 0b10000,
        LTIMER = 0b10001,
    }
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Control0(u32);
    impl Debug;
    pub efuse_key, set_efuse_key: 31;
    pub icap_select, set_icap_select: 30;
    pub over_temp_power_down, set_over_temp_power_down: 12;
    pub config_fallback, set_config_fallback: 10;
    pub glutmask_b, set_glutmask_b: 8;
    pub farsrc, set_farsrc: 7;
    pub dec, set_dec: 6;
    pub sbits, set_sbits: 5, 4;
    pub persist, set_persist: 3;
    pub gts_usr_b, set_gts_usr_b: 0;
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct Status(u32);
    impl Debug;
    pub bus_width, set_but_width: 26, 25;
    pub startup_state, set_startup_state: 20, 18;
    pub dec_error, set_dec_error: 16;
    pub id_error, set_id_error: 15;
    pub done, set_done: 14;
    pub release_done, set_release_done: 13;
    pub init_b, set_init_b: 12;
    pub init_complete, set_init_complete: 11;
    pub mode, set_mode: 10, 8;
    pub ghigh_b, set_ghigh_b: 7;
    pub gwe, set_gwe: 6;
    pub gts_cfg_b, set_gts_cfg_b: 5;
    pub eos, set_eos: 4;
    pub dci_match, set_dci_match: 3;
    pub mmcm_lock, set_mmcm_lock: 2;
    pub part_secured, set_part_secured: 1;
    pub crc_error, set_crc_error: 0;
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct ConfigurationOptions0(u32);
    impl Debug;
    pub pwrdwn_stat, set_pwrdwn_stat: 27;
    pub done_pipe, set_done_pipe: 25;
    pub drive_done, set_drive_done: 24;
    pub single, set_single: 23;
    pub oscfsel, set_oscfsel: 22, 17;
    pub ssclksrc, set_ssclksrc: 16, 15;
    pub done_cycle, set_done_cycle: 14, 12;
    pub match_cycle, set_match_cycle: 11, 9;
    pub lock_cycle, set_lock_cycle: 8, 6;
    pub gts_cycle, set_gts_cycle: 5, 3;
    pub gwe_cycle, set_gwe_cycle: 2, 0;
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct ConfigurationOptions1(u32);
    impl Debug;
    pub persist_deassert_at_desync, set_persist_deassert_at_desync: 17;
    pub rbcrc_action, set_rbcrc_action: 16, 15;
    pub rbcrc_no_pin, set_rbcrc_no_pin: 9;
    pub rbcrc_en, set_rbcrc_en: 8;
    pub bpi_1st_read_cycle, set_bpi_1st_read_cycle: 3, 2;
    pub bpi_page_size, set_bpi_page_size: 1, 0;
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct WarmBootStartAdress(u32);
    impl Debug;
    pub rs, set_rs: 31, 30;
    pub rs_ts_b, set_rs_ts_b: 29;
    pub start_addr, set_start_addr: 28, 0;
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct WatchdogTimer(u32);
    impl Debug;
    pub timer_usr_mon, set_timer_usr_mon: 31;
    pub timer_cfg_mon, set_timer_cfg_mon: 30;
    pub timer_value, set_timer_value: 29, 0;
}

bitfield! {
    #[derive(Copy, Clone, PartialEq, Eq, Hash)]
    pub struct BootHistoryStatus(u32);
    impl Debug;
    pub hmac_error_1, set_hmac_error_1: 15;
    pub wrap_error_1, set_wrap_error_1: 14;
    pub crc_error_1, set_crc_error_1: 13;
    pub id_error_1, set_id_error_1: 12;
    pub wto_error_1, set_wto_error_1: 11;
    pub iprog_1, set_iprog_1: 10;
    pub fallback_1, set_fallback_1: 9;
    pub valid_1, set_valid_1: 8;
    pub hmac_error_0, set_hmac_error_0: 7;
    pub wrap_error_0, set_wrap_error_0: 6;
    pub crc_error_0, set_crc_error_0: 5;
    pub id_error_0, set_id_error_0: 4;
    pub wto_error_0, set_wto_error_0: 3;
    pub iprog_0, set_iprog_0: 2;
    pub fallback_0, set_fallback_0: 1;
    pub valid_0, set_valid_0: 0;
}
