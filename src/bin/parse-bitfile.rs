extern crate failure;
extern crate series7;

use std::io;

fn main() {
    if let Err(err) = run() {
        eprintln!("{}", err);
    }
}

fn run() -> Result<(), failure::Error> {
    let r = series7::bitfile::header_from_reader(io::stdin())?;
    println!("design name: {}", r.design_name);
    println!("part name: {}", r.part_name);
    println!("date: {}", r.date);
    println!("time: {}", r.time);

    Ok(())
}
