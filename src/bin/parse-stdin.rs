extern crate failure;
extern crate series7;
#[macro_use]
extern crate structopt;

use series7::bitstream;
use std::io;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "parse-stdin", about = "parse a bitstream from stdin")]
enum Command {
    #[structopt(name = "packets")]
    Packets,
    #[structopt(name = "raw-packets")]
    RawPackets,
    #[structopt(name = "register-writes")]
    RegisterWrites,
}

fn main() {
    if let Err(err) = run() {
        eprintln!("{}", err);
    }
}

fn run() -> Result<(), failure::Error> {
    let command = Command::from_args();
    let mut stdin = io::stdin();
    match command {
        Command::Packets => {
            let packets = bitstream::packets_from_reader(&mut stdin)?;
            println!("{:#?}", packets);
        }
        Command::RawPackets => {
            let raw_packets = bitstream::raw_packets_from_reader(&mut stdin)?;
            println!("{:#?}", raw_packets);
        }
        Command::RegisterWrites => {
            let register_writes = bitstream::register_writes_from_reader(&mut stdin)?;
            println!("{:#?}", register_writes);
        }
    };

    Ok(())
}
