extern crate failure;
extern crate series7;
extern crate tempfile;

use series7::bitstream;
use series7::bitstream::raw_packet::Header;
use series7::bitstream::register::{FrameAddress, RegisterAddress};
use series7::bitstream::Packet;
use series7::bitstream::RegisterWrite;
use series7::frames::FRAME_LENGTH;
use series7::vivado::Vivado;
use std::collections::HashMap;
use std::fmt::Display;
use std::fs::File;
use std::io::Read;
use std::str;
use tempfile::tempdir;
use tempfile::NamedTempFile;

// For some reason, if we try to build bitfiles for more than 16 part, vivado fails with "WARNING: [Device 21-150] Attempt to create more than 16 speed controllers"
const MAX_PART_PER_INVOCATION: usize = 16;

const MINIMAL_VHDL: &str = r#"
Library UNISIM;
use UNISIM.vcomponents.all;

entity top is end top;

architecture arch of top is
attribute keep : string;
attribute keep of  LUT1_inst : label is "true";

begin
   LUT1_inst : LUT1
   generic map (
      INIT => "00")
   port map (
      I0 => '0'
   );
end;
"#;

fn main() {
    if let Err(err) = run() {
        eprintln!("{}", err);
    }
}

fn make_bitstreams(
    vhdl_path: impl Display,
    bitstreams_dir_path: impl Display,
    parts: &[&str],
) -> Result<(), failure::Error> {
    let mut tcl = String::new();
    for part in parts {
        tcl += &format!(
            r#"
         read_vhdl {vhdl}
         synth_design -top top -part {part}
         place_design -directive Quick
         route_design
         set_property BITSTREAM.GENERAL.DEBUGBITSTREAM Yes  [current_design]
         write_bitstream -force {dir}/{part}
         "#,
            vhdl = vhdl_path,
            dir = bitstreams_dir_path,
            part = part
        )
    }

    Vivado::new().with_tcl(tcl).run()?;
    Ok(())
}

fn debug_bitstream_to_idcode_and_addresses<R: Read>(
    reader: &mut R,
) -> Result<(u32, Vec<Option<FrameAddress>>), failure::Error> {
    let raw_packets = bitstream::raw_packets_from_reader(reader)?;

    let mut addresses = Vec::new();
    let mut type_0_count = 0;
    let mut first_lout_read = false;
    for packet in &raw_packets {
        if let Header::Other(0) = packet.header {
            type_0_count += 1;
            if type_0_count == FRAME_LENGTH {
                addresses.push(None);
                type_0_count = 0;
            }
        } else if let Some(RegisterAddress::LOUT) = packet.header.register_address() {
            type_0_count = 0;
            if first_lout_read {
                let address = *packet.payload.first().unwrap_or(&0);
                addresses.push(Some(FrameAddress(address)));
            } else {
                first_lout_read = true;
            }
        }
    }

    let packets = Packet::from_raw_packets(raw_packets.into_iter());
    let register_writes = RegisterWrite::from_packets(packets.into_iter())?;
    let idcode = register_writes
        .into_iter()
        .filter_map(|r| match r {
            RegisterWrite::Idcode(idcode) => Some(idcode),
            _ => None,
        })
        .next()
        .unwrap_or(0);

    Ok((idcode, addresses))
}

fn split(s: &str) -> (&str, &str) {
    let idx = s.find(' ').unwrap_or(0);

    let (s1, s2) = s.split_at(idx);

    (s1, &s2[1..])
}

fn run() -> Result<(), failure::Error> {
    let bitstreams_dir = tempdir()?;

    let bitstreams_dir_path = bitstreams_dir.path();
    let bitstreams_dir_path_str = bitstreams_dir_path.display();

    let parts = series7::vivado::Vivado::new()
        .with_tcl("foreach part [get_parts -regexp x.7.* ] {set device [get_property device $part]; puts $output \"$device $part\"}")
        .run_with_output()?;
    let parts: Vec<_> = parts
        .split(|c| *c == b'\n')
        .filter(|s| !s.is_empty())
        .map(str::from_utf8)
        .map(|r| r.map(split))
        .collect::<Result<_, _>>()?;

    let mut device_to_part_names = HashMap::new();

    for (device, part_name) in &parts {
        device_to_part_names
            .entry(device)
            .or_insert(Vec::new())
            .push(part_name);
    }

    println!("{:?}", device_to_part_names);

    let vhdl_file = NamedTempFile::new()?;
    let vhdl_file_path = vhdl_file.path().display();

    std::fs::write(&vhdl_file, MINIMAL_VHDL)?;

    let parts: Vec<_> = device_to_part_names
        .values()
        .map(|v| *(*v.first().unwrap()))
        .collect();

    for parts_chunks in parts.chunks(MAX_PART_PER_INVOCATION) {
        make_bitstreams(&vhdl_file_path, &bitstreams_dir_path_str, parts_chunks)?;

        for part in parts_chunks.iter() {
            let bitstream_path = bitstreams_dir_path.join(format!("{}.bit", part));
            let mut bitstream_file = File::open(bitstream_path)?;
            let (idcode, addresses) = debug_bitstream_to_idcode_and_addresses(&mut bitstream_file)?;

            println!("------------ {:?}, {}, {}", part, idcode, addresses.len());
        }
    }

    Ok(())
}
