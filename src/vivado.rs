use std::fs;
use std::io;
use std::io::Write;
use std::process::Command;
use std::process::Stdio;
use tempfile::tempdir;
use tempfile::NamedTempFile;

#[derive(Clone)]
pub struct Vivado {
    tcl: String,
}

impl Vivado {
    pub fn new() -> Vivado {
        Vivado { tcl: String::new() }
    }

    pub fn with_tcl<T: Into<String>>(&mut self, tcl: T) -> &mut Vivado {
        self.tcl = tcl.into();
        self
    }

    pub fn run(&self) -> Result<(), VivadoError> {
        let tempdir = tempdir()?;
        let mut child = Command::new("vivado")
            .args(&["-nolog", "-nojournal", "-mode", "tcl", "-tempDir"])
            .arg(tempdir.path())
            .stdin(Stdio::piped())
            .spawn()?;

        {
            let stdin = child.stdin.as_mut().unwrap();
            stdin.write_all(self.tcl.as_bytes())?;
        }

        let exit_status = child.wait()?;

        if exit_status.success() {
            Ok(())
        } else {
            Err(VivadoError::ExitFailure)
        }
    }

    pub fn run_with_output(&self) -> Result<Vec<u8>, VivadoError> {
        let output_file = NamedTempFile::new()?;

        let new_tcl = format!(
            "set output [open \"{}\" \"w\"]\n{}",
            output_file.path().display(),
            self.tcl
        );
        self.clone().with_tcl(new_tcl).run()?;

        Ok(fs::read(output_file)?)
    }
}

#[derive(Debug, Fail)]
pub enum VivadoError {
    #[fail(display = "io error: {}", _0)]
    IoError(#[cause] io::Error),
    #[fail(display = "Vivado exit status was not a success")]
    ExitFailure,
}

impl From<io::Error> for VivadoError {
    fn from(value: io::Error) -> VivadoError {
        VivadoError::IoError(value)
    }
}
