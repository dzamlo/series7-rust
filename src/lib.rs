#[macro_use]
extern crate bitfield;
extern crate byteorder;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate nom;
extern crate tempfile;

#[macro_use]
pub mod utils;

pub mod bitfile;
pub mod bitstream;
pub mod devicedb;
pub mod frames;
pub mod vivado;
