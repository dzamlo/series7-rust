use bitfield::fmt::Formatter;
use std::cmp::Ordering;
use std::fmt;
use std::hash::Hash;
use std::hash::Hasher;
use std::ops::BitAnd;
use std::ops::BitAndAssign;
use std::ops::BitOr;
use std::ops::BitOrAssign;
use std::ops::BitXor;
use std::ops::BitXorAssign;
use std::ops::Index;
use std::ops::IndexMut;
use std::ops::Not;
use std::slice::Iter;
use std::slice::IterMut;

pub const FRAME_LENGTH: usize = 101;

#[derive(Clone)]
pub struct Frame(pub [u32; FRAME_LENGTH]);

impl Frame {
    fn as_slice(&self) -> &[u32] {
        &self.0[..]
    }

    pub fn iter(&self) -> Iter<u32> {
        self.0.iter()
    }

    pub fn iter_mut(&mut self) -> IterMut<u32> {
        self.0.iter_mut()
    }
}

impl fmt::Debug for Frame {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        fmt.debug_tuple("Frame").field(&self.as_slice()).finish()
    }
}

impl Default for Frame {
    fn default() -> Self {
        Frame([0; FRAME_LENGTH])
    }
}

impl Index<usize> for Frame {
    type Output = u32;

    fn index(&self, index: usize) -> &u32 {
        &self.0[index]
    }
}

impl IndexMut<usize> for Frame {
    fn index_mut(&mut self, index: usize) -> &mut u32 {
        &mut self.0[index]
    }
}

impl PartialEq for Frame {
    fn eq(&self, other: &Frame) -> bool {
        self.as_slice() == other.as_slice()
    }
}

impl Eq for Frame {}

impl Hash for Frame {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.as_slice().hash(state)
    }
}

impl Ord for Frame {
    fn cmp(&self, other: &Self) -> Ordering {
        (*self.as_slice()).cmp(other.as_slice())
    }
}

impl PartialOrd for Frame {
    fn partial_cmp(&self, other: &Frame) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Not for Frame {
    type Output = Frame;

    fn not(mut self) -> <Self as Not>::Output {
        self.iter_mut().for_each(|n| *n = !*n);
        self
    }
}

macro_rules! impl_op {
    ($trait:ident, $method:ident, $traitAssign:ident, $methodAssign:ident, $op:tt) => {
        impl $trait for Frame {
            type Output = Frame;

            fn $method(mut self, rhs: Frame) -> Frame {
                self $op rhs;
                self
            }
        }

        impl<'a> $trait<&'a Frame> for Frame {
            type Output = Frame;

            fn $method(mut self, rhs: &'a Frame) -> Frame {
                self $op rhs;
                self
            }
        }

        impl $traitAssign for Frame {
            fn $methodAssign(&mut self, rhs: Frame) {
                for (lhs, rhs) in self.iter_mut().zip(rhs.iter()) {
                    *lhs $op rhs;
                }
            }
        }

        impl<'a> $traitAssign<&'a Frame> for Frame {
            fn $methodAssign(&mut self, rhs: &'a Frame) {
                for (lhs, rhs) in self.iter_mut().zip(rhs.iter()) {
                    *lhs $op rhs;
                }
            }
        }

    };
}

impl_op! {BitAnd, bitand, BitAndAssign, bitand_assign, &=}
impl_op! {BitOr, bitor, BitOrAssign, bitor_assign, |=}
impl_op! {BitXor, bitxor, BitXorAssign, bitxor_assign, ^=}
